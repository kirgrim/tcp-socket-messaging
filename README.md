# Tcp Socket Messaging

Program showing basic interaction between TCP client and server sockets. Server socket can accept infinite number of client sockets but processing is sequential due to the PIL. 

**TODO**: 
- include multiprocessing
- allow client terminate connection with 'quit' command.
- SSL support.

# Client Intructions
**In order to achieve desired effect user should**:
- adopt host and port to user's need (in client_config.yaml file)
- run programm
- enter 's' in terminal in order to get user's statistic since last call (this will erase user's character history)

# Server Intructions
**In order to achieve desired effect user should**:
- adopt port and ascii_range to user's need (in server_config.yaml file)
- run programm
- spectate connected clients in terminal