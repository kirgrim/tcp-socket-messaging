import random
import socket
import sys
import threading
import time
import yaml


def parse_config(file_path):
    with open(file_path) as server_config_file:
        parsed_yaml_file = yaml.load(server_config_file, Loader=yaml.FullLoader)
        try:
            if parsed_yaml_file['config']['ascii_range']:
                ascii_range = tuple(
                    [max(0, min(int(x), 255)) for x in parsed_yaml_file['config']['ascii_range'].split(',') if x])
            else:
                ascii_range = (0, 255)
            if len(ascii_range) == 0:
                ascii_range = (0, 255)
            elif len(ascii_range) == 1:
                print('Invalid interval specified')
                sys.exit(-1)
            server_port = parsed_yaml_file['config']['server_port']
            return server_port, ascii_range
        except ValueError as e:
            print('Terminated with value error: ', e)
            sys.exit(-1)


def interact_with_client(conn, charlist, addr):
    start = time.time()
    random_interval = random.randint(0, 3)

    def get_commands_from_client():
        while True:
            try:
                data = conn.recv(1024)
                if str(data.decode('ascii')) == 's':
                    if charlist and len(charlist) > 0:
                        conn.send(str.encode('{N} characters were sent out since last call'.format(N=len(charlist))))
                        charlist.clear()
                    else:
                        conn.send(str.encode('No data was transmitted yet'))
                print('Received from {address} : {data}'.format(address=addr, data=str(data.decode('ascii'))))
            except ConnectionResetError:
                break

    client_data_receiver = threading.Thread(target=get_commands_from_client, args=())
    client_data_receiver.start()

    while True:
        try:
            if time.time() - start > random_interval:
                start = time.time()
                random_chr = chr(random.randrange(ascii_range[0], ascii_range[1]))
                charlist.append(random_chr)
                conn.send(str.encode(random_chr))
                random_interval = random.randint(0, 3)
        except ConnectionResetError:
            print('Connection aborted with {address}'.format(address=addr))
            connected_clients.pop(addr, None)
            break


connected_clients = dict()

if __name__ == '__main__':
    server_port, ascii_range = parse_config('server_config.yaml')
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as err:
        print("Socket creation failed with error: ", err)
        sys.exit()
    try:
        s.bind(('', server_port))
        s.listen(1)
        while True:
            c, addr = s.accept()
            print('Received connection from', addr[0], ':', addr[1])
            connected_clients[addr] = []
            client_interactor = threading.Thread(target=interact_with_client, args=(c, connected_clients[addr], addr))
            client_interactor.start()
    except ValueError as e:
        print('Error: Value provided is not integer: ', server_port)
        sys.exit()
