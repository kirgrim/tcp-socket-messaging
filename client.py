import socket
import sys
import threading

import yaml


def parse_config(file_path):
    with open(file_path) as server_config_file:
        parsed_yaml_file = yaml.load(server_config_file, Loader=yaml.FullLoader)
        try:
            server_host = parsed_yaml_file['config']['server_host']
            server_port = parsed_yaml_file['config']['server_port']
            return server_host, server_port
        except ValueError as e:
            print('Terminated with value error: ', e)
            sys.exit(-1)


def input_thread():
    while True:
        command = input()
        s.send(str.encode(command))


'''TODO: Add SSL layer for sockets'''
# hostname = 'www.python.org'
# context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
# context.load_verify_locations('cert.pem')

if __name__ == '__main__':
    server_host, server_port = parse_config('client_config.yaml')
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error as err:
        print("Socket creation failed with error: ", err)
        sys.exit()
    try:
        host_ip = socket.gethostbyname(server_host)
    except socket.gaierror:
        print("There was an error resolving the host: ", server_host)
        sys.exit()
    try:
        try:
            s.connect((host_ip, server_port))
            threading.Thread(target=input_thread, args=()).start()
            while True:
                data = s.recv(1024)
                print('Received from the server :', str(data.decode('ascii')))
        except ConnectionRefusedError as e:
            print('Server aborted connection')
            sys.exit()
    except ValueError as e:
        print('Value provided is not an integer: ', server_port)
        sys.exit()
